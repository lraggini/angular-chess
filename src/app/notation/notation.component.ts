import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-notation',
  templateUrl: './notation.component.html',
  styleUrls: ['./notation.component.css']
})
export class NotationComponent implements OnInit {
  @Input() game: Position[];
  constructor() { }

  ngOnInit() {
  }

}
