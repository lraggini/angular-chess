import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { WhiteQueenComponent } from './pieces/white-queen/white-queen.component';
import { BlackQueenComponent } from './pieces/black-queen/black-queen.component';
import { BlackPawnComponent } from './pieces/black-pawn/black-pawn.component';
import { WhitePawnComponent } from './pieces/white-pawn/white-pawn.component';
import { WhiteKingComponent } from './pieces/white-king/white-king.component';
import { BlackKingComponent } from './pieces/black-king/black-king.component';
import { WhiteBishopComponent } from './pieces/white-bishop/white-bishop.component';
import { BlackBishopComponent } from './pieces/black-bishop/black-bishop.component';
import { WhiteKnightComponent } from './pieces/white-knight/white-knight.component';
import { BlackKnightComponent } from './pieces/black-knight/black-knight.component';
import { WhiteRookComponent } from './pieces/white-rook/white-rook.component';
import { BlackRookComponent } from './pieces/black-rook/black-rook.component';
import { PromoteDialogComponent } from './promote-dialog/promote-dialog.component';
import { NotationComponent } from './notation/notation.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    WhiteQueenComponent,
    BlackQueenComponent,
    BlackPawnComponent,
    WhitePawnComponent,
    WhiteKingComponent,
    BlackKingComponent,
    WhiteBishopComponent,
    BlackBishopComponent,
    WhiteKnightComponent,
    BlackKnightComponent,
    WhiteRookComponent,
    BlackRookComponent,
    PromoteDialogComponent,
    NotationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  entryComponents: [
    PromoteDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
