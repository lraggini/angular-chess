import { Component } from '@angular/core';
import { Position } from './models/Position';
import { Positions } from './models/Positions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  position = new Position(
    [
      -4,-2,-3,-5,-6,-3,-2,-4,
      -1,-1,-1,-1,-1,-1,-1,-1,
       0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0,
       1, 1, 1, 1, 1, 1, 1, 1,
       4, 2, 3, 5, 6, 3, 2, 4
    ],
    true,
    [60, 4],
    [true, true],
    [true, true],
    -1,
    ''
  );
  positions: Positions;
  game: Position[];
  moves: string[];

  constructor() {
    this.positions = new Positions(this.position);
    this.game = new Array();
    this.game.push(this.position);
    this.moves= new Array();
  }
}
