import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Position } from '../models/Position';
import { Positions } from '../models/Positions';

import { PromoteDialogComponent } from '../promote-dialog/promote-dialog.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  @ViewChild('hidden') hidden: ElementRef;

  @Input() position: Position;
  @Input() game: Position[];

  positions: Positions;

  currentPiece: number;
  currentIndex: number;

  touchmoveListenFunc: Function = null;
  touchendListenFunc: Function = null;
  touchcancelListenFunc: Function = null;

  constructor(
    public renderer: Renderer2,
    public promoteDialog: MatDialog
  ) {
    this.currentPiece = 0;
    this.currentIndex = 0;    
  }

  ngOnInit() {
    this.positions = new Positions(this.position);
    this.positions.generate();
  }

  @HostListener('document:mousemove', ['$event'])
  @HostListener('document:touchmove', ['$event'])
  onMove(e) {
    if (this.currentPiece !== 0) {
      this.setHiddenToMousePosition(e);
      if(this.isOutOfBoardBounds(e)) {
        this.position = this.positions.base;
        this.hideHidden();
      }
    }
  }

  mousedown(i: number, e): void {
    if (this.isPieceMovable(i)) {
      if (e.touches) {
        this.removePreviousTouchListeners();
        this.touchmoveListenFunc = this.renderer.listen(
          e.target,
          'touchmove',
          (event) => { this.onMove(event);
        });
        this.touchendListenFunc = this.renderer.listen(
          e.target,
          'touchend',
          (event) => { this.removePreviousTouchListeners(); this.touchend(event);
        });
        this.touchcancelListenFunc = this.renderer.listen(
          e.target,
          'touchcancel',
          (event) => { this.removePreviousTouchListeners(); this.touchend(event);
        });
      }
      const tempPiece: number = this.position.diag[i];

      this.positions = new Positions(this.position);
      this.positions.generate();

      this.position.diag[i] = 0;
      this.currentPiece = tempPiece;
      this.currentIndex = i;

      this.hidden.nativeElement.style.display = 'block';
      this.setHiddenToMousePosition(e);
    }
  }

  removePreviousTouchListeners() {
    if (this.touchmoveListenFunc !== null) {
      this.touchmoveListenFunc();
    }
    if (this.touchendListenFunc !== null) {
      this.touchendListenFunc();
    }
    if (this.touchcancelListenFunc !== null) {
      this.touchcancelListenFunc();
    }

    this.touchmoveListenFunc = null;
    this.touchendListenFunc = null;
    this.touchcancelListenFunc = null;
  }

  mouseup(i: number, e: MouseEvent): void {
    if (this.currentPiece !== 0) {
      this.handleMouseUp(i);
      this.hideHidden();    
    }
  }

  touchend(e: TouchEvent): void {
    if (this.currentPiece !== 0) {
      const width = this.hidden.nativeElement.offsetWidth;
      if (width !== 0) {
        const i = Math.trunc(e.changedTouches[0].clientX / width);
        const j = Math.trunc(e.changedTouches[0].clientY / width);  
        this.handleMouseUp(i + j * 8);            
      }
      this.hideHidden();
    }
  }

  getSquareClass(i: number): string {
    const x = i % 8;
    const y = Math.trunc(i / 8);

    return ((x + y) + 1) % 2 === 0 ? 'black' : '';
  }

  private openPromoteDialog(color: boolean, i: number, j: number): void {
    const dialogRef = this.promoteDialog.open(PromoteDialogComponent, {
      width: (this.hidden.nativeElement.offsetWidth * 4) + 48 + 'px',
      data: color
    });
    dialogRef.afterClosed().subscribe(result => {
      this.position.diag[i] = result;   
      this.position.diag[j] = 0;
      const index = this.isPositionPossible();      
      if (index !== -1) {
        this.position = this.positions.list[index];        
        this.positions.base = this.position;        
        this.positions.generate();
      } else {
        this.position = this.positions.base;
      }
    });
  }

  private handleMouseUp(i: number) {
    this.position.diag[i] = this.currentPiece;
    this.handleEnPassant(i);
    this.handleCastle(i);
    this.handlePromote(i, this.currentIndex);        
    const index = this.isPositionPossible();
    if (index !== -1) {
      this.position = this.positions.list[index];      
      this.game.push(this.position);
      this.positions.base = this.position;          
      this.positions.generate();
    } else {          
      this.position = this.positions.base;
    }
  }

  private handlePromote(i: number, j: number): void {
    if (this.currentPiece === 1 && this.currentIndex > 7 && this.currentIndex < 16 && i < 8 && this.isPromotePossible(i, j)) {            
      this.openPromoteDialog(true, i, j);
    }
    if (this.currentPiece === -1 && this.currentIndex > 47 && this.currentIndex < 56 && i > 55 && this.isPromotePossible(i, j)) {
      this.openPromoteDialog(false, i, j);
    }
  }

  private handleCastle(i: number): void {
    if (this.currentPiece === 6 && this.currentIndex === 60 && i === 62) {
      this.position.diag[63] = 0; 
      this.position.diag[61] = 4;
    }
    if (this.currentPiece === 6 && this.currentIndex === 60 && i === 58) {
      this.position.diag[56] = 0; 
      this.position.diag[59] = 4;
    }
    if (this.currentPiece === -6 && this.currentIndex === 4 && i === 6) {
      this.position.diag[7] = 0; 
      this.position.diag[5] = -4;
    }
    if (this.currentPiece === -6 && this.currentIndex === 4 && i === 2) {
      this.position.diag[0] = 0; 
      this.position.diag[3] = -4;
    } 
  }
  
  private handleEnPassant(i: number): void {    
    if (this.currentPiece === 1 && i > 15 && i < 23 && this.position.diag[i + 8] === -1 && this.positions.base.diag[i] === 0) {
      this.position.diag[i + 8] = 0;
    }
    if (this.currentPiece === -1 && i > 39 && i < 48 && this.position.diag[i - 8] === 1 && this.positions.base.diag[i] === 0) {
      this.position.diag[i - 8] = 0;
    }
  }

  private isPromotePossible(i: number, j: number) {
    for (let x = 0; x < this.positions.list.length; x++) {
      if (this.positions.list[x].diag[j] === 0 
       && ((!this.positions.list[x].trait && [2, 3, 4, 5].indexOf(this.positions.list[x].diag[i]) !== -1) ||
           (this.positions.list[x].trait && [-2, -3, -4, -5].indexOf(this.positions.list[x].diag[i]) !== -1))
      ) {
        return true;
      }
    }
    return false;
  }

  private isPieceMovable(i: number) {
    for (let x = 0; x < this.positions.list.length; x++) {
      if (this.positions.list[x].diag[i] === 0) {
        return true;
      }
    }
    return false;
  }  

  private isPositionPossible(): number {
    for (let x = 0; x < this.positions.list.length; x++) {
      let isEqual: boolean = true;
      for (let y = 0; y < 64; y++) {
        if (this.positions.list[x].diag[y] !== this.position.diag[y]) {
          isEqual = false;
          break;
        }
      }
      if (isEqual) {
        return x;
      }
    }
    return -1;
  }

  private setHiddenToMousePosition(e): void {
    const width = this.hidden.nativeElement.offsetWidth;
    const x = e.clientX || e.touches[0].clientX;
    const y = e.clientY || e.touches[0].clientY;
    this.hidden.nativeElement.style.top = (y - width / 2) + 'px';
    this.hidden.nativeElement.style.left = (x - width / 2) + 'px';
  }

  private hideHidden(): void {
    this.currentPiece = 0;
    this.currentIndex = 0;
    this.hidden.nativeElement.style.display = 'none';
  }

  private isOutOfBoardBounds(e): boolean {
    const width = this.hidden.nativeElement.offsetWidth;
    const x = e.clientX || e.touches[0].clientX;
    const y = e.clientY || e.touches[0].clientY;
    return x < 0 || y < 0 || x > 8 * width || y > 8 * width;
  }
}
