import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackQueenComponent } from './black-queen.component';

describe('BlackQueenComponent', () => {
  let component: BlackQueenComponent;
  let fixture: ComponentFixture<BlackQueenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackQueenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackQueenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
