import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteKingComponent } from './white-king.component';

describe('WhiteKingComponent', () => {
  let component: WhiteKingComponent;
  let fixture: ComponentFixture<WhiteKingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteKingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteKingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
