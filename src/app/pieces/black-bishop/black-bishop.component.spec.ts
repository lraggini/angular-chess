import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackBishopComponent } from './black-bishop.component';

describe('BlackBishopComponent', () => {
  let component: BlackBishopComponent;
  let fixture: ComponentFixture<BlackBishopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackBishopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackBishopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
