import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteQueenComponent } from './white-queen.component';

describe('WhiteQueenComponent', () => {
  let component: WhiteQueenComponent;
  let fixture: ComponentFixture<WhiteQueenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteQueenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteQueenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
