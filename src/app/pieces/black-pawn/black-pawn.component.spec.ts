import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackPawnComponent } from './black-pawn.component';

describe('BlackPawnComponent', () => {
  let component: BlackPawnComponent;
  let fixture: ComponentFixture<BlackPawnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackPawnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackPawnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
