import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteBishopComponent } from './white-bishop.component';

describe('WhiteBishopComponent', () => {
  let component: WhiteBishopComponent;
  let fixture: ComponentFixture<WhiteBishopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteBishopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteBishopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
