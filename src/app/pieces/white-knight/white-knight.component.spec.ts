import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteKnightComponent } from './white-knight.component';

describe('WhiteKnightComponent', () => {
  let component: WhiteKnightComponent;
  let fixture: ComponentFixture<WhiteKnightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteKnightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteKnightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
