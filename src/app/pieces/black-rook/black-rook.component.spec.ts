import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackRookComponent } from './black-rook.component';

describe('BlackRookComponent', () => {
  let component: BlackRookComponent;
  let fixture: ComponentFixture<BlackRookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackRookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackRookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
