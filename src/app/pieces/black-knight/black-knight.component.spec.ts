import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackKnightComponent } from './black-knight.component';

describe('BlackKnightComponent', () => {
  let component: BlackKnightComponent;
  let fixture: ComponentFixture<BlackKnightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackKnightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackKnightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
