import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackKingComponent } from './black-king.component';

describe('BlackKingComponent', () => {
  let component: BlackKingComponent;
  let fixture: ComponentFixture<BlackKingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackKingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackKingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
