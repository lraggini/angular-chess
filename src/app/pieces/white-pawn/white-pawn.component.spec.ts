import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhitePawnComponent } from './white-pawn.component';

describe('WhitePawnComponent', () => {
  let component: WhitePawnComponent;
  let fixture: ComponentFixture<WhitePawnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhitePawnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhitePawnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
