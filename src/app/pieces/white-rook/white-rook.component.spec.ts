import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteRookComponent } from './white-rook.component';

describe('WhiteRookComponent', () => {
  let component: WhiteRookComponent;
  let fixture: ComponentFixture<WhiteRookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteRookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteRookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
