import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-promote-dialog',
  templateUrl: './promote-dialog.component.html',
  styleUrls: ['./promote-dialog.component.css']
})
export class PromoteDialogComponent implements OnInit {
  color: boolean;
  constructor(    
    public dialogRef: MatDialogRef<PromoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: boolean
  ) {
    this.color = data;
  }

  ngOnInit() {
  }

  choosePiece(i: number): void {
    this.dialogRef.close(i);
  }
}
